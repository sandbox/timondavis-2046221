<?php
/**
 * @file
 * tasks_and_timers.module
 *
 * Main module file for Tasks and Timers module
 * Main logic controller and function provider
 */

/**
 * Load Core Classes and supports
 */
module_load_include('inc', 'tasks_and_timers', 'class/Timer.class');
module_load_include('inc', 'tasks_and_timers', 'class/TaskCalculator.class');
module_load_include('inc', 'tasks_and_timers', 'tasks_and_timers.forms');
module_load_include('inc', 'tasks_and_timers', 'tasks_and_timers.pages');
module_load_include('inc', 'tasks_and_timers', 'views/tasks_and_timers.views');

define('TASKS_AND_TIMERS_TASK_NODE_TYPE', 'task');

/**
 * Implements hook_permission().
 */
function tasks_and_timers_permission() {

  return array(
    'use timer' => array(
      'title' => t('Use Timer'),
      'description' => t('Allows use of the timer control form, time reporting.')
    ),
    'view time reports' => array(
      'title' => t('View Time Reports'),
      'description' => t('Allows user to review all time reports'),
    ),
    'view task summary' => array(
      'title' => t('View Task Summary'),
      'description' => t('Allow user to review the task summary screen'),
    ),
    'alter any timer' => array(
      'title' => t('Alter any timer'),
      'description' => t('Allow user to alter or confirm any timer'),
    ),
  );
}

/**
 * Implements hook_theme().
 */
function tasks_and_timers_theme($existing) {

  return array(
    'views_calc_table__task_time_report' => array(
      'path' => drupal_get_path('module', 'tasks_and_timers') . '/views/theme',
      'arguments' => array('view' => NULL, 'options' => NULL, 'rows' => NULL),
      'template' => 'views-calc-table--task-time-report',
      'original hook' => 'views-calc-table',
    ),
    'views_calc_table__user_time_report' => array(
      'path' => drupal_get_path('module', 'tasks_and_timers') . '/views/theme',
      'arguments' => array('view' => NULL, 'options' => NULL, 'rows' => NULL),
      'template' => 'views-calc-table--user-time-report',
      'original hook' => 'views-calc-table',
    ),
  );
}

/**
 * Call outside template to fix issues with task views calc in this scenario.
 *
 * @see http://your.sitename.com/time-report/task/%nid
 */
function tasks_and_timers_preprocess_views_calc_table__task_time_report(&$vars) {

  template_preprocess_views_calc_table($vars);
}

/**
 * Call outside template to fix issues with views calc in this scenario.
 *
 * @see http://your.sitename.com/time-report/user/%uid
 */
function tasks_and_timers_preprocess_views_calc_Table__user_time_report(&$vars) {

  template_preprocess_views_calc_table($vars);
}

/**
 * Implements hook_node_info().
 */
function tasks_and_timers_node_info() {

  return array(
    'task' => array(
      'base' => 'task',
      'name' => t('Task'),
      'description' => 'A Task',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function tasks_and_timers_menu() {

  $menu = array();

  $menu['user/%user/confirm-timers'] = array(
    'description' => 'Confirm the time that a given user has executed',
    'page callback' => 'tasks_and_timers_confirm_user_time_page',
    'page arguments' => array(1),
    'access callback' => 'tasks_and_timers_confirm_access',
    'access arguments' => array(1),
  );

  // Calculate Task Time and return vital data.
  $menu['confirm-timers/calculate-task-time'] = array(
    'page callback' => 'tasks_and_timers_calculate_task_time',
    'access callback' => TRUE,
    'delivery callback' => 'drupal_json_output',
    'type' => MENU_CALLBACK,
  );

  // Confirm timer and lock into place.
  $menu['confirm-timers/confirm/%tasks_and_timers_timer'] = array(
    'page callback' => 'tasks_and_timers_confirm_timer',
    'access callback' => TRUE,
    'page arguments' => array(2),
    'delivery callback' => 'drupal_json_output',
    'type' => MENU_CALLBACK,
  );

  // Remove timer from system.
  $menu['confirm-timers/remove/%tasks_and_timers_timer'] = array(
    'page callback' => 'tasks_and_timers_remove_timer',
    'access callback' => TRUE,
    'page arguments' => array(2),
    'delivery callback' => 'drupal_json_output',
    'type' => MENU_CALLBACK,
  );

  return $menu;
}

/**
 * Loads up and returns a timer object with the indicated Timer ID.
 * 
 * @param int $timer_id 
 *   The Timer ID to load
 * 
 * @return Timer 
 *   The loaded timer, or NULL if id is invalid
 * 
 * @see class/Timer.class.inc
 */
function tasks_and_timers_timer_load($timer_id) {

  $timer = NULL;

  try {

    $timer = new Timer($timer_id);

    return $timer;
  }
  catch (Exception $ex) {

    watchdog('tasks_and_timers',
      'Timer Id @timer_id is not available', array('@timer_id' => $timer_id),
      NULL, WATCHDOG_NOTICE);
    drupal_set_message($ex->getMessage, 'error');

    return NULL;
  }
}

/**
 * Implements hook_views_api().
 */
function tasks_and_timers_views_api() {

  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'tasks_and_timers') . '/views',
  );
}

/**
 * Implements hook_block_info().
 */
function tasks_and_timers_block_info() {

  $blocks = array();

  $blocks['timer_controls'] = array(
    'info' => t('Timer Controls'),
    'cache' => DRUPAL_NO_CACHE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function tasks_and_timers_block_view($delta = '') {

  $block = array();

  switch ($delta) {

    case ('timer_controls'):{

      // Stop unauthorized users from seeing the timer form.
      if (user_access('use timer')) { 

        $node = menu_get_object('node');

        if ($node) {

          $block['subject'] = t('Timer Controls');
          $block['content']
            = drupal_get_form('tasks_and_timers_timer_control_form');
        }
      }

      break;
    }
  }

  return $block;
}

/**
 * Get the term id of the project assigned to a Task node.
 * 
 * @param int $nid
 *   The nid (node id) of the Task Node in question.
 * 
 * @return int
 *   Returns the term id (tid) assigned to the Task Node or NULL if not found.
 */
function tasks_and_timers_get_current_project_tid($nid) {

  $query = db_select('field_data_field_task_project', 'ftp')
         ->fields('ftp', array('field_task_project_tid'))
         ->condition('entity_type', 'node')
         ->condition('entity_id', $nid);

  $result = $query->execute();

  if ($result && $record = $result->fetchAssoc()) {

    return $record['field_task_project_tid'];
  }

  return NULL;
}

/**
 * Get the Taxonomy Terms for Phase terms attributed to a Project term.
 * 
 * @param int $project_tid
 *   The Project Term ID (tid) in question.
 * 
 * @return int
 *   An array of Phase Terms belonging to the Project passed in. $tid => $term.
 */
function tasks_and_timers_get_phase_terms_in_project($project_tid) {

  if (!is_numeric($project_tid)) {

    return NULL;
  }

  $query = db_select('field_data_field_task_project', 'ftp')
         ->fields('ftp', array('entity_id'))
         ->condition('entity_type', 'taxonomy_term')
         ->condition('field_task_project_tid', $project_tid);

  $result = $query->execute();

  if ($result) {

    $resultset = array();

    while ($record = $result->fetchAssoc()) {

      $resultset[$record['entity_id']]
        = taxonomy_term_load($record['entity_id']);
    }

    return $resultset;
  }

  return NULL;
}

/**
 * Given elapsed time in seconds, returns elapsed time in H:i:s format.
 * 
 * @param int $seconds
 *   Amount of time passed (elapsed time), in seconds.
 *
 * @return string
 *   Amount of hours, minutes and seconds passed in UNIX H:i:s format.
 */
function tasks_and_timers_convert_elapsed_time($seconds) {

  // Return value.
  $ret = "";

  // Get the hours.
  $hours = intval(intval($seconds) / 3600);

  if ($hours > 0) {

    $ret .= $hours . ":";
  }
  else {

    $ret .= '00:';
  }

  // Get the minutes.
  $minutes = bcmod((intval($seconds) / 60), 60);

  if ($hours > 0 || $minutes > 0) {

    $ret .= sprintf('%02d', $minutes) . ":";
  }
  else {

    $ret .= '00:';
  }

  // Get the seconds.
  $seconds = bcmod(intval($seconds), 60);
  $ret .= sprintf('%02d', $seconds);

  return $ret;
}

/**
 * Implements hook_node_delete().
 *
 * Cleanup the tt_timers table after a node has been deleted.
 */
function tasks_and_timers_node_delete($node) {

  $nid = $node->nid;

  $num_deleted = db_delete('tt_timers')
               ->condition('task_id', $nid)
               ->execute();
}
