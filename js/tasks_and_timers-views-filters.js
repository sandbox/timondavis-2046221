/**
 * @file
 *  Alteration to existing logic that ultimately prevents ajax
 *  calss from a views filter form.
 */
(function($) {
  Drupal.ajax.prototype.beforeSubmit = 
  function (form_values, element, options) { 

    $('select:disabled option').each(function() { 
      form_values.push({'name':$(this).val(), 'value':$(this).text()});
    });
  }
})(jQuery);
