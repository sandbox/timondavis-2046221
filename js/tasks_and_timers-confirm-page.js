
/**
 * @file
 *   Actively maintains timer data, makes AJAX calls to drupal site to 
 *   confirm, update and remove timers.
 * 
 * @author 
 *   Timon Davis <timondavis@yahoo.com>
 */
(function($) {
  /**
   * Convert a number of seconds into years, months, weeks, days, hrs, mins.
   *
   * I lifted this formatInterval method from the Drupal stack overflow.
   * Converts raw second into a astring which denotes elapsed time.
   *
   * @original-author http://drupal.stackexchange.com/users/2800/clive
   * 
   * @param int interval
   *   The number of seconds passed in a given time interval.
   * @param int granularity
   *   The level of granularity, which determines how many item-details will
   *   be in the returned string.
   * @param int langcode
   *   Language handling parameter
   *
   * @return string
   *   Returns a string with the amount of time passed, in human readable form.
   *
   * @see http://drupal.stackexchange.com/questions/73635/which-is-the-equivalent-of-php-drupal-format-interval-for-javascript
   */
  Drupal.formatInterval = function(interval, granularity, langcode) {
    granularity = typeof granularity !== 'undefined' ? granularity : 2;
    langcode = typeof langcode !== 'undefined' ? langcode : null;
  
    // Specify available unit outputs and values.
    var units = {
      '1 year|@count years': 31536000,
      '1 month|@count months': 2592000,
      '1 week|@count weeks': 604800,
      '1 day|@count days': 86400,
      '1 hour|@count hours': 3600,
      '1 min|@count min': 60,
      '1 sec|@count sec': 1
    },

    // Initialize output.
    output = '';
  
    // Parase out time units of time available, 
    // Attribute time passed to each unit until granularity
    // is exhausted.
    for (var key in units) {
      var keys = key.split('|'); 
      var value = units[key];
      if (interval >= value) {
        output += (output.length ? ' ' : '') + Drupal.formatPlural(Math.floor(interval / value), keys[0], keys[1], {}, { langcode: langcode });
        interval %= value;
        granularity--;
      }
  
      if (granularity == 0) {
        break;
      }
    }
  
    // Return formatted output.
    return output.length ? output : Drupal.t('0 sec', {}, { langcode: langcode });
  }

  /**
   * Add logic to form fields and links that update both the page and the 
   * server with current information.
   */
  Drupal.behaviors.confirmTimers = {
    attach:function(context, settings) {

      // Identify all of the triggering elements and bind them to 
      // the appropriate update method.
      $(".confirm-timer-date-collection div input").bind("blur", function(event) { 

        updateTimers(event);
      });

      $(".confirm-link").bind("click", function(event) { 

        confirmTimer(event);
      });

      $(".remove-link").bind("click", function(event) { 

        removeTimerByLink(event);
      });
    }
  };

  /**
   * A quick tool to return only unique
   * values in an array.
   */
  Array.prototype.getUnique = function () { 

    var u = {}, a = [];

    for (var i = 0 ; l = this.length, i < l ; i++) { 

      if (u.hasOwnProperty(this[i])) { 

        continue;
      }

      a.push(this[i]);
      u[this[i]] = 1;
    }

    return a;
  }
      
  /**
   * Remove a timer block from the page, based on timer id.
   *
   * @param Object data
   *   Object which repressents a Task Timer
   */
  var removeTimer = function(data) { 

    if (data.success) { 

      var id = data.timer_id;
      var byeByeDiv = $("#" + id + "_timer_table");
      $(byeByeDiv).remove();
    }
  }

  /**
   * Tell drupal to delete the Timer in question.
   * 
   * Call out to Drupal and give it the order to remove the 
   * triggering element's timer from the system.
   *
   * @param Event event
   *   Javascript Event object.
   */
  function removeTimerByLink(event) { 

    var targetInput = event.target;
    var taskInfo = getTaskInfoByInput(targetInput, true);
 
    var timerId = taskInfo.timer_id;
    var timerData = getTimerDataByTimerId(timerId);

    $.ajax({
      type: 'POST',
      url: Drupal.settings.basePath + 'confirm-timers/remove/' + timerId,
      success: removeTimer,
      data: 'js=1&time_data=' + JSON.stringify(timerData),
    });
  }

  /**
   * Tell drupal to confirm the Timer in question.
   *
   * @param Event event
   *  Javascipt Event object.
   */
  function confirmTimer(event) { 

    var targetInput = event.target;
    var taskInfo = getTaskInfoByInput(targetInput, true); 

    var timerId = taskInfo.timer_id;
    var timerData = getTimerDataByTimerId(timerId);
    
    $.ajax({ 
      type: 'POST',
      url: Drupal.settings.basePath + 'confirm-timers/confirm/' + timerId,
      dataType: 'json',
      success: removeTimer,
      data: 'js=1&time_data=' + JSON.stringify(timerData),
    });
  }

  /**
   * Convenience method to aid in capturing task information from the DOM.
   * 
   * Given a Timer Input element from the confirm form, 
   * returns the standard information array about the group it
   * belongs to.  Contains stuff like timer id, task id, etc
   *
   * This method is really in prtotype right now, made specifically to grab
   * elemetns from a very specific DOM strucuture.
   *
   * @param targetInput
   *   The input element to query against
   * @param smallrange
   *   narrows the tareting algorithm.
   */
  function getTaskInfoByInput(targetInput, smallrange) { 

    if (smallrange == null) { 

      smallrange = false;
    }

    if (smallrange) { 

      var parentDivs = $(targetInput).parent().parent().find(".timer-information");
      var encodedTaskInfoElement = parentDivs[0];
      var taskInfo = JSON.parse($(encodedTaskInfoElement).val());

      return taskInfo;
    }

    else {  

      var parentDiv = $(targetInput).parent().parent().parent();
      var encodedTaskInfoElement = parentDiv.children(".timer-information");
      var taskInfo = JSON.parse($(encodedTaskInfoElement).val());

      return taskInfo
    }

    
    return null;
  }

  /**
   * Given a Timer Input element from the confirm form,
   * returns the task ID to which it belongs
   */
  function getTaskIdByInput(targetInput) { 

    var taskInfo = getTaskInfoByInput(targetInput);

    return taskInfo.task_id;
  }

  /**
   * Get all of the timer ids that associate with the given task id
   *
   * @param int taskId
   *   The Task ID, or Drupal NID, of the Task to query against.
   *
   * @return Array
   *   Returns an array of timer ids
   */
  function collectTimerIdsByTaskId(taskId) { 

    // Collect all timer info elements
    var timerInfoElements = $("input.timer-information");

    // Create a container for releveant timer ids
    var timerIdCollection = Array();

    // Traverse through all of the timer info data packets
    // and pick out timer ids that belong to the relevant task id
    // passed in
    timerInfoElements.each(function(index, element) { 

      // Parse the json packet
      var timerInfo = JSON.parse($(element).val());

      // Add the timer id if the task id is a match
      if (timerInfo.task_id == taskId) { 

        timerIdCollection.push(timerInfo.timer_id);
      }
    });

    // Dedupe results and return to caller
    return timerIdCollection.getUnique();
  }

  /**
   * Returns timer data given the timer's ID
   *
   * @param int timerId
   *   The ID of the timer in question
   *
   * @return Object
   *   Returns a data object with start and finishing
   *   timer statistics.
   */
  function getTimerDataByTimerId(timerId) { 

    var data = {};

    var startDivId = timerId + "_start_time";
    var finishDivId = timerId + "_finish_time";

    var startElement = $("div#" + startDivId);
    var finishElement = $("div#" + finishDivId);

    data.start = getTimerDataByParentDiv(startElement);
    data.finish = getTimerDataByParentDiv(finishElement);
    data.timer_id = timerId;

    return data;
  }

  /**
   * A private convenience function to get Timer Data from the Form.
   *
   * @param element
   *   The parent element to the desired data structure.
   *
   * @reutrn
   *  Returns time and date arrays containing user input.
   */
  function getTimerDataByParentDiv(element) {

    var timeDiv = element.find("form div div.timer-time");
    var dateDiv = element.find("form div div.timer-date");

    var timeInputs = timeDiv.find("div input");
    var dateInputs = dateDiv.find("div input");

    var time = {};
    var date = {};

    timeInputs.each(function(index, element) {

      time[index] = $(element).val();
    });

    dateInputs.each(function(index, element) { 

      date[index] = $(element).val();
    });

    var result = {};

    result.time = time;
    result.date = date;

    return result;
  }

  /**
   * Handles successful timer update to Drupal, updates times on form page.
   *
   * @param data
   *   AJAX data package
   */
  var updateTimerOutput = function (data) {

    var elapsedTime = data.elapsed_time;
    var timerData = data.timer_data;

    for (var property in timerData) { 

      var timerTable = $("#" + property + "_timer_table");
      var timerElapsedTd = $(timerTable).find("td.timer_elapsed_time");
      var taskTotalTd = $(timerTable).find("td.task_total_time");

      var timerTime = timerData[property];
      timerElapsedTd.text(timerTime);
      taskTotalTd.text(elapsedTime);
    }
  }

  /**
   * Event handler bound to all confirm timer inputs by default
   */
  function updateTimers(event) { 

    // Get the task ID to which the given input belonged to.
    var targetInput = event.target;
    var taskId = getTaskIdByInput(targetInput);

    // Collect all other timers that share this task id
    var taskTimers = collectTimerIdsByTaskId(taskId);
    var timerData = Array();

    $(taskTimers).each(function(index, element) { 

      timerData[index] = getTimerDataByTimerId(element);
    });

    // Encode timerData into json and make a call to the server
    // time processor.  Display updated results.  Do NOT confirm any of this time.
    $.ajax({ 
      type: 'POST',
      url: Drupal.settings.basePath + 'confirm-timers/calculate-task-time',
      dataType: 'json',
      success: updateTimerOutput,
      data: 'js=1&time_data=' + JSON.stringify(timerData), 
    });
  }
})(jQuery); 
