/**
 * @file
 *   Actively maintains animated timers on Timer Control Form.
 */
(function($) {

  /**
   * Convert a number of seconds into years, months, weeks, days, hrs, mins.
   *
   * I lifted this formatInterval method from the Drupal stack overflow.
   * Converts raw second into a astring which denotes elapsed time.
   *
   * @original-author http://drupal.stackexchange.com/users/2800/clive
   * 
   * @param int interval
   *   The number of seconds passed in a given time interval.
   * @param int granularity
   *   The level of granularity, which determines how many item-details will
   *   be in the returned string.
   * @param int langcode
   *   Language handling parameter
   *
   * @return string
   *   Returns a string with the amount of time passed, in human readable form.
   *
   * @see http://drupal.stackexchange.com/questions/73635/which-is-the-equivalent-of-php-drupal-format-interval-for-javascript
   */
  Drupal.formatInterval = function(interval, granularity, langcode) {
    granularity = typeof granularity !== 'undefined' ? granularity : 2;
    langcode = typeof langcode !== 'undefined' ? langcode : null;
  
    var units = {
      '1 year|@count years': 31536000,
      '1 month|@count months': 2592000,
      '1 week|@count weeks': 604800,
      '1 day|@count days': 86400,
      '1 hour|@count hours': 3600,
      '1 min|@count min': 60,
      '1 sec|@count sec': 1
    },
    output = '';
  
    for (var key in units) {
      var keys = key.split('|'); 
      var value = units[key];
      if (interval >= value) {
        output += (output.length ? ' ' : '') + Drupal.formatPlural(Math.floor(interval / value), keys[0], keys[1], {}, { langcode: langcode });
        interval %= value;
        granularity--;
      }
  
      if (granularity == 0) {
        break;
      }
    }
  
    return output.length ? output : Drupal.t('0 sec', {}, { langcode: langcode });
  }

  Drupal.behaviors.activateTimers = {
    attach:function(context, settings) {

      var getSplitTimerInput = function (activeTimeString) { 

        var activeTimeArray = activeTimeString.split(" ");
        var activeTimeReportArray = new Array();

        var componentValue = null;

        for (var i = 0 ; i < activeTimeArray.length ; i ++) { 

          // Test if even
          var even = (i % 2 == 0 || i == 0) ? true : false; 

          if (even) { 

            componentValue = activeTimeArray[i];
          }
          else { 

            activeTimeReportArray[activeTimeArray[i]] = componentValue;
          }
        }

        return activeTimeReportArray;
      }

      var getSplitTimerSeconds = function (timeReportArray, uptick) { 

        if(uptick == null) { 

          uptick = 0;
        }

        var accumulatedSeconds = 0;

        if ("sec" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["sec"]);
        }

        if ("min" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["min"] * 60);
        }

        if ("hour" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["hour"] * 3600);
        }
        else if ("hours" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["hours"] * 3600);
        }

        if ("day" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["day"] * 86400);
        }
        else if ("days" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["days"] * 86400);
        }

        if ("week" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["week"] * 604800);
        }
        else if ("weeks" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["weeks"] * 604800);
        }

        if ("month" in timeReportArray) { 
        
          accumulatedSeconds += parseInt(timeReportArray["month"] * 2592000);
        }
        else if ("months" in timeReportArray) { 
        
          accumulatedSeconds += parseInt(timeReportArray["months"] * 2592000);
        }

        if ("year" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["year"] * 31536000);
        }
        else if ("years" in timeReportArray) { 

          accumulatedSeconds += parseInt(timeReportArray["years"] * 31536000);
        }
    
        return accumulatedSeconds;
      }

      // First, find out if there is an active timer at all
      // We don't have any further interest if there is not 
      // an active timer.
      var isTimerActive = $("input[name='has_active_timer']").attr("value");
      if (isTimerActive == 1) {  
        
        // Get the time from the clock
        var activeTime = $("#activeTimer").text();
        var activeTimerArray = getSplitTimerInput(activeTime);
        var activeTimerSeconds = getSplitTimerSeconds(activeTimerArray); 

        var updateActiveTimer = function() { 

           activeTimerSeconds++;
           $("#activeTimer").text(Drupal.formatInterval(activeTimerSeconds, 4));
        };

        var activeTimer = 0;
        activeTimer = setInterval(updateActiveTimer, 1000); 

        var activeHistoryTime = $('#activeHistoryTimer').text();
        var activeHistoryTimerArray = getSplitTimerInput(activeHistoryTime);
        var activeHistorySeconds = getSplitTimerSeconds(activeHistoryTimerArray);

        var updateActiveHistoryTimer = function() { 

          activeHistorySeconds++;
          $("#activeHistoryTimer").text( Drupal.formatInterval(activeHistorySeconds, 4));
        }

        var activeHistoryTimer = 0;
        activeHistoryTimer = setInterval(updateActiveHistoryTimer, 1000); 
      }

      var isTaskActive = $("input[name='this_task_active']").attr("value");

      if (isTaskActive == 1) { 

        var currentTaskTotalTime = $('#taskTotalTimer').text();
        var currentTaskTotalTimerArray = getSplitTimerInput(currentTaskTotalTime);
        var currentTaskTotalSeconds = getSplitTimerSeconds(currentTaskTotalTimerArray);

        var updateCurrentTaskTotal = function() { 

          currentTaskTotalSeconds++;          
          $("#taskTotalTimer").text( Drupal.formatInterval(currentTaskTotalSeconds, 4));
        }

        var currentTaskTotalTimer = 0;
        currentTaskTotalTimer = setInterval(updateCurrentTaskTotal, 1000); 


        var currentTaskTodayTime = $('#taskTodayTimer').text();
       
        var currentTaskTodayTimerArray = getSplitTimerInput(currentTaskTodayTime);
        var currentTaskTodaySeconds = getSplitTimerSeconds(currentTaskTodayTimerArray);

        var updateCurrentTaskToday = function() { 

         currentTaskTodaySeconds++;
          $("#taskTodayTimer").text( Drupal.formatInterval(currentTaskTodaySeconds, 4));
        }

        var currentTaskTodayTimer = 0;
        currentTaskTodayTimer = setInterval(updateCurrentTaskToday, 1000); 
      }
    }
  };
})(jQuery); 
