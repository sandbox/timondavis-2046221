<?php
/**
 * @file
 * Manages form generation, structure and modifications.
 * 
 * @author Timon Davis <timondavis@yahoo.com>
 */

/**
 * Implements hook_form().
 *
 * Create the timer confirmation form (One form per timer).
 * 
 * Requires 3 arguments in call embedded in form_state args 
 * (which should happen automatically if drupal_get_form 
 * invokes this function with the three arguments).
 *
 * $time : int      This is the timestamp value.
 * $timer_id : int  This is the timer id from the database row.
 * $instance_type : string   This is arbitrary as long as its unique to 
 *                             your purpose
 */
function tasks_and_timers_timer_confirm_form($form, $form_state) {

  // Collect args and extract data.
  $args = $form_state['build_info']['args'];

  $time = $args[0];
  $timer_id = $args[1];
  $instance_type = $args[2];
  $timer = new Timer($timer_id);

  // Create the form.
  $form = array();

  // Convert passed time to 'm/d/y H:i:s' structure.
  $month = date('m', $time);
  $day = date('d', $time);
  $year = date('Y', $time);
  $hour = date('H', $time);
  $minute = date('i', $time);
  $second = date('s', $time);

  // Create context for the form.  Include js and css files.
  $form['#prefix'] = '<div id="' . $timer_id . '_' . $instance_type . '">';
  $form['#suffix'] = '</div>';
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'tasks_and_timers') .
    '/css/tasks_and_timers-confirm-form.css');

  // JSON Encode timer data in hidden element for JS consumption.
  $form['timer_information'] = array(
    '#type' => 'hidden',
    '#value' => json_encode(array(
      'timer_id' => $timer_id,
      'task_id'  => $timer->getTask()->nid,
      'instance_type' => $instance_type,
    )),
    '#attributes' => array(
      'class' => 'timer-information',
    ),
  );

  // Create input fields.
  // Dump the current values into form inputs as default values.
  $form['start_month_' . $timer_id] = array(
    '#prefix' => '<div class="confirm-timer-date-collection timer-date">',
    '#suffix' => '<span class="confirm-timer-date-collection-slash">/</span>',
    '#type' => 'textfield',
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => $month,
  );

  $form['start_day_' . $timer_id] = array(
    '#suffix' => '<span class="confirm-timer-date-collection-slash">/</span>',
    '#type' => 'textfield',
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => $day,
  );

  $form['start_year_' . $timer_id] = array(
    '#suffix' => '</div><br/>',
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#default_value' => $year,
  );

  $form['start_hour_' . $timer_id] = array(
    '#prefix' => '<div class="confirm-timer-date-collection timer-time">',
    '#suffix' => '<span class="confirm-timer-date-collection-slash">:</span>',
    '#type' => 'textfield',
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => $hour,
  );

  $form['start_minute_' . $timer_id] = array(
    '#suffix' => '<span class="confirm-timer-date-collection-slash">:</span>', '#type' => 'textfield',
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => $minute,
  );

  $form['start_second' . $timer_id] = array(
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#size' => 2,
    '#maxlength' => 2,
    '#default_value' => $second,
  );

  return $form;
}

/**
 * Implements hook_form().
 *
 * Creates the timer control form.
 */
function tasks_and_timers_timer_control_form() {

  // Create new form.
  $form = array();

  // Collect Page Statistics.
  $node = menu_get_object();
  $active_node = NULL;

  // Collect User Statistics.
  global $user;
  $uid = $user->uid;

  // Reject form load if we don't have the required materials.
  // We must have a node object with the 'nid' property.
  if (!is_object($node) || !property_exists($node, 'nid')) {

    return;
  }

  // Indicate whether or not the user has an active timer.
  $form['has_active_timer'] = array(
    '#type' => 'hidden',
    '#value' => (Timer::getActiveTimer($uid)) ? 1 : 0,
  );

  // If there is an active timer, get the node with which it is associated.
  if ($form['has_active_timer']['#value']) {
    $active_node = Timer::getActiveTimer($uid)->getTask();
  }

  // Indicate whether or not the current case is the active case
  // for the viewing user.
  $form['this_task_active'] = array(
    '#type' => 'hidden',
    '#value' => _is_this_task_active($node, $uid),
  );

  // Store some basic info about the node and timer.
  $form['nid'] = array(
    '#type' => 'hidden',
    '#value' => $node->nid,
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $uid,
  );

  // Define submit handler (must be explicit as form lives in block).
  $form['#submit'][] = 'tasks_and_timers_timer_control_form_submit';

  // Attach JS and CSS for look/feel/behavior.
  $form['#attached']['css'] = array(
    drupal_get_path('module', 'tasks_and_timers') .
    '/css/tasks_and_timers-timer-controls.css');
  $form['#attached']['js'] = array(
    drupal_get_path('module', 'tasks_and_timers') .
    '/js/tasks_and_timers-timer-form.js');

  // Print information about active timer.
  $form['active_timer_data'] = array(
    '#prefix' => '<div id="myActiveTimer">',
    '#markup' => _active_timer_markup($active_node, $uid),
    '#suffix' => '</div>',
  );

  // Put up a stop button if there is an active task.
  if ($active_node) {

    $form['stop'] = array(
      '#prefix' => '<div class="timer_buttons">',
      '#type' => 'submit',
      '#value' => t('Stop Active Task'),
      '#suffix' => '</div>',
    );
  }

  // Print information about timer on currently
  // viewed task.
  _current_task_markup($form, $node, $uid);
  _append_summary_markup($form, $uid);

  // Return form to caller.
  return $form;
}

/**
 * Implements hook_form_submit().
 *
 * Submission handler for Timer Control Form.
 */
function tasks_and_timers_timer_control_form_submit($form, &$form_state) {

  // Collect form state values.
  $values = $form_state['values'];

  // Collect essential info.  'op' indicates which button was pushed on the
  // form.
  $op = $values['op'];
  $uid = $values['uid'];
  $user = user_load($uid);
  $nid = $values['nid'];

  // Take action based on which button was clicked.
  switch ($op) {

    case ('Start This Task'):{

      // Kill whatever timer was running (if any) and start a new timer.
      $timer = Timer::startNewTimer($user->uid, $nid);

      break;
    }

    case ('Stop Active Task'):{

      // Stop the active timer.
      $timer = Timer::getActiveTimer($uid);

      if (!$timer) {

        // Take no action.
      }
      else {

        $timer->stop();
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function tasks_and_timers_form_alter(&$form, $form_state, $form_id) {

  $form['additional_settings']['#access'] = FALSE;

  switch ($form_id) {

    // Add some logic to the Task Node Add form which manages the
    // relationship between projects and phases.
    case ('task_node_form'):{

      // Narrow range of phase options based on selected project.
      $form['field_task_project'][LANGUAGE_NONE]['#ajax'] = array(
        'callback' => 'tasks_and_timers_update_available_projects',
        'wrapper' => 'phases-available',
        'method' => 'replace',
        'style' => 'fade',
      );

      $form['field_task_phase']['#prefix'] = '<div id="phases-available">';
      $form['field_task_phase']['#suffix'] = '</div>';

      $new_options = tasks_and_timers_get_current_project_phase_options(
        $form, $form_state);

      $form['field_task_phase'][LANGUAGE_NONE]['#options'] = $new_options;

      break;
    }

    // Add some logic to Tasks and Timers views which manage the
    // relationship between projects and phases.
    case ('views_exposed_form'):{

      if ($form['#id'] == 'views-exposed-form-tasks-page') {

        $form['#attached']['js'] = array(
          drupal_get_path('module', 'tasks_and_timers') .
          '/js/tasks_and_timers-views-filters.js');

        $form['field_task_project_tid']['#ajax'] = array(
          'callback' => 'tasks_and_timers_update_tasks_view',
          'wrapper' => 'views-phases-available',
          'method' => 'replace',
          'style' => 'fade',
        );

        $form['field_task_phase_tid']['#prefix']
          = '<div id="views-phases-available">';
        $form['field_task_phase_tid']['#suffix'] = '</div>';

        $form['form_information'] = array(
          '#prefix' => '<div style="display:none">',
          '#suffix' => '</div>',
          '#type' => 'select',
          '#disabled' => TRUE,
          '#options' => array(
            'form_id' => $form['#form_id'],
            'form_build_id' => $form['#build_id'],
            'form_token' => $form['#token'],
          ),
        );
      }

      break;
    }

    default:
      break;
  }
}

/**
 * Provides appropriate phases given a current assigned task.
 * 
 * Takes form and form state from task edit form, returns 
 * back appropriate options for phase dropdown.
 *
 * This method can handle any form containing field_task_project, 
 * where such a field is used to contain term references to the provided 
 * Project vocabulary.
 *
 * @param array $form
 *   Drupal Form Array belonging to any form 
 * @param array $form_state
 *   Metadata about the Drupal Form Array and user inputs
 * @param bool $is_view_filter_form
 *   (optional) Pass in TRUE if you're trying to get the options on a views 
 *   filter form.
 */
function tasks_and_timers_get_current_project_phase_options($form, $form_state,
  $is_view_filter_form = FALSE) {

  if (!$is_view_filter_form) {

    $current_project_tid = 0;

    if (array_key_exists('values', $form_state) &&
        $form_state['values']['field_task_project']) {

      $current_project_tid
        = $form_state['values']['field_task_project'][LANGUAGE_NONE][0]['tid'];
    }
    elseif (property_exists($form['#node'], 'nid')) {

      tasks_and_timers_get_current_project_tid($form['#node']->nid);
    }
  }

  else {

    $current_project_tid = $form_state['values']['field_task_project_tid'];
  }

  $available_phases
    = tasks_and_timers_get_phase_terms_in_project($current_project_tid);

  $new_options = array();

  if (!$is_view_filter_form) {

    $new_options['_none'] = '- None -';
  }
  else {

    $new_options['All'] = '- Any -';
  }

  foreach ($available_phases as $tid => $phase) {

    $new_options[$tid] = $phase->name;
  }

  return $new_options;
}

/**
 * Generate markup about the active timer for the given user.
 *
 * @todo:  submit theme logic to theme layer and execute logic outside
 */
function _active_timer_markup($node, $uid) {

  // Initialize locals.
  $active_timer_markup = '';

  $active_timer_markup = '<h3 class="timer_section_label">Active Task</h3>';

  // Get the user's active timer, if it exists.
  $user_active_timer = Timer::getActiveTimer($uid);

  // Load up timer statistics if there is a timer.
  if ($user_active_timer) {

    $timers = TaskCalculator::collectTimers($node->nid, $uid);
    $timers_today = TaskCalculator::sumRecordsFromDateRange(
      strtotime('today'), strtotime('now'), $uid, $node->nid);
    $time_today = (array_key_exists($node->nid, $timers_today)) ?
      $timers_today[$node->nid]['sum'] : 0;

    // Add active timer if it didn't come up in the 'today' search.
    if ($user_active_timer && $user_active_timer->getStartTime() <
      strtotime('today')) {

      $time_today += strtotime('now') - $user_active_timer->getStartTime();
    }

    // Get the node which the timer refers to.
    $active_node = $user_active_timer->getTask();

    // Get the user's timer history on this case.
    $active_node_history
      = TaskCalculator::collectTimers($active_node->nid, $uid);
    $active_node_time = TaskCalculator::sum($active_node_history);

    // Build markup.
    $active_timer_markup .= '<div class="timer_label_active timer_task_link">';
    $active_timer_markup .= l($active_node->title,
                              'node/' . $active_node->nid);
    $active_timer_markup .= '</div>';
    $active_timer_markup .= '<div class="timer_label_active">Time Today</div>';
    $active_timer_markup .= '<div id="activeTimer" class="timer_time">' .
      format_interval($time_today, 4) . '</div>';
  }
  else {

    $active_timer_markup .= '<div class="timer_label_inactive">No timer is ' .
      'active</div>';
  }

  return $active_timer_markup;
}

/**
 * Generate Markup about the currently viewed task by the given user.
 *
 * @todo:  submit theme logic to theme layer and execute logic outside
 */
function _current_task_markup(&$form, $node, $uid) {

  // Collect THIS Timer's Statistics.
  $timers = TaskCalculator::collectTimers($node->nid, $uid);
  $timers_today = TaskCalculator::sumRecordsFromDateRange(
    strtotime('today'), strtotime('now'), $uid, $node->nid);
  $time_today = (array_key_exists($node->nid, $timers_today)) ?
    $timers_today[$node->nid]['sum'] : 0;

  // Get the user's active timer, if it exists.
  $user_active_timer = Timer::getActiveTimer($uid);

  // Add active timer if it didn't come up in the 'today' search.
  if ($user_active_timer && $user_active_timer->getStartTime() <
    strtotime('today')) {

    $time_today += strtotime('now') - $user_active_timer->getStartTime();
  }

  $current_task_markup_today = '';
  $current_task_markup_total = '';

  if ($timers) {

    $form['current_task_info'] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('Current Task'),
      '#prefix' => '<div id="myDailyHistory">',
      '#suffix' => '</div>',
    );

    $current_task_markup_today .= '<div class="timer_label_active">Time Today</div>';
    $current_task_markup_today .= '<br/><div id="taskTodayTimer" class="timer_time">';
    $current_task_markup_today .= format_interval($time_today, 4);
    $current_task_markup_today .= '</div>';

    $current_task_markup_total .= '<div class="timer_label_active">Time To-Date</div>';
    $current_task_markup_total .= '<br/><div id="taskTotalTimer" class="timer_time">';
    $current_task_markup_total .=
      format_interval(TaskCalculator::sum($timers), 4);
    $current_task_markup_total .= '</div>';
  }
  else {

    $current_task_markup_today .= 'You have not started a timer on this task';
  }

  $form['current_task_info']['time_today'] = array(
    '#markup' => $current_task_markup_today,
  );

  $form['current_task_info']['time_total'] = array(
    '#markup' => $current_task_markup_total,
  );

  // Start and stop buttons (wrapped in a div for
  // form control).
  if (!$user_active_timer) {

    $form['current_task_info']['start'] = array(
      '#type' => 'submit',
      '#value' => t('Start This Task'),
      '#prefix' => '<div class="timer_buttons">',
      '#suffix' => '</div>',
    );
  }
}

/**
 * Test whether or not tasks is active for given user.
 * 
 * Used by the form to determine whether the current task is also 
 * the task for the viewed node.  Returns 1 and 0 instead of TRUE/FALSE.
 * as this value is used to communicate with JS in the browser
 * 
 * @param object $node
 *   Drupal node object.
 * @param int $uid
 *   UID of drupal user.
 *
 * @return int
 *   0/1 to represent TRUE/FALSE. 1 if active, 0 if not.
 */
function _is_this_task_active($node, $uid) {

  $active_timer = Timer::getActiveTimer($uid);

  if ($active_timer) {

    if ($active_timer->getTask()->nid == $node->nid) {

      return 1;
    }
  }

  return 0;
}

/**
 * Append form markup with formatted data.
 * 
 * Used by the form to collect information about the daily history of the
 * given user.  
 * 
 * @todo: should really be handled by theme layer, after passing in
 * control variables which could still be determined in this method
 */
function _append_summary_markup(&$form, $uid) {

  if (!$today_history = TaskCalculator::sumRecordsFromDateRange(
    strtotime('today'), strtotime('now'), $uid)) {

    return;
  }

  // Print Daily Work Summary.
  $form['summary'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t("Today's History"),
    '#prefix' => '<div id="myDailyHistory">',
    '#suffix' => '</div>',
  );

  $today_history = TaskCalculator::sumRecordsFromDateRange(
    strtotime('today'), strtotime('now'), $uid);

  foreach ($today_history as $key => $history) {

    // History returns a 'sum' element, which we will
    // not display here.
    if ($key == 'sum') {

      continue;
    }

    if (Timer::getActiveTimer($uid) &&
        $history['nid'] == Timer::getActiveTimer($uid)->getTask()->nid) {

      if (Timer::getActiveTimer($uid)->getStartTime() < strtotime('today')) {

        $history['sum'] +=
          strtotime('now') - Timer::getActiveTimer($uid)->getStartTime();
      }

      $form['summary'][$history['title']] = array(
        '#markup' => '<div class="timer_label_active">' .
        l($history['title'], 'node/' . $history['nid']) . '</div>' .
        '<div class="timer_label_active">Time Today</div><br/>' .
        '<div id="activeHistoryTimer" class="timer_time">' .
        format_interval($history['sum'], 4) .
        '</div>');

    }
    else {
      $form['summary'][$history['title']] = array(
        '#markup' => '<div class="timer_label_active">' .
        l($history['title'], 'node/' . $history['nid']) . '</div>' .
        '<div class="timer_label_active">Time Today</div><br/>' .
        '<div class="timer_time">' .
        format_interval($history['sum'], 4) .
        '</div>',
      );
    }
  }
}
