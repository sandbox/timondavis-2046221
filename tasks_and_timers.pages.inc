<?php

/**
 * JSON DATA Callback
 * Removes timer from system.
 */
function tasks_and_timers_remove_timer($timer) { 

  if ($timer) { 

    $timer->delete();

    return array (
      'success' => TRUE,
      'timer_id' => $timer->getTimerID(),
    );
  }

  return array (
    'success' => FALSE,
    'timer_id' => FALSE,
  );
}

/**
 * JSON DATA Callback
 * Confirms the timer as printed in the browser.
 */
function tasks_and_timers_confirm_timer($timer) {

  // Reject if no array key
  if(!array_key_exists('time_data', $_POST)) return;

  // Collect abasic data
  global $user;
  $active_user = clone($user);
  $time_data = json_decode($_POST['time_data']);

  $msg_arr = array();

  if ($timer) { 

    $start_time_string = '';
    $finish_time_string = '';

    _append_time_string($time_data->start, $start_time_string);
    _append_date_string($time_data->start, $start_time_string);

    _append_time_string($time_data->finish, $finish_time_string);
    _append_date_string($time_data->finish, $finish_time_string);

    $timer->setStartTime(strtotime($start_time_string));
    $timer->setFinishTime(strtotime($finish_time_string)); 
    $timer->save();

    $timer->confirm($active_user->uid);

    // setup return object
    $msg_arr['success'] = TRUE;
    $msg_arr['timer_id'] = $timer->getTimerID();
  }

  else { 

    $msg_arr['success'] = FALSE;
    $msg_arr['timer_id'] = FALSE;
  }

  return $msg_arr;
}

/**
 * Access Callback for confirmation page.
 */
function tasks_and_timers_confirm_access($url_user) {
  global $user;

  if (($user->uid == $url_user->uid && user_access('use timer')) || user_access('alter any timer')) {

    return TRUE;
  }

  return FALSE;
}

/**
 * Calculate time based on json-string passed in
 */
function tasks_and_timers_calculate_task_time() { 

  $time_data = json_decode($_POST['time_data']);

  $accumulated_time = 0;
  $timer_data = array();

  foreach($time_data as $timer) { 

    $start_time_string = '';
    $finish_time_string = '';

    _append_time_string($timer->start, $start_time_string);
    _append_date_string($timer->start, $start_time_string);

    _append_time_string($timer->finish, $finish_time_string);
    _append_date_string($timer->finish, $finish_time_string);

    $timer_id = $timer->timer_id;

    $elapsed_time = strtotime($finish_time_string) - strtotime($start_time_string);
    $timer_data[$timer_id] = tasks_and_timers_convert_elapsed_time($elapsed_time);
    $accumulated_time += $elapsed_time;
  }

  $return_data['timer_data'] = $timer_data;

  $printed_time_elapsed = tasks_and_timers_convert_elapsed_time($accumulated_time);
  $return_data['elapsed_time'] = $printed_time_elapsed;

  return $return_data;
}


/**
 * Update row callback (depricated??)
 */
function tasks_and_timers_update_row_callback($timer_id, $start_time, $finish_time) { 

  $timer_query_result = db_select('tt_timers', 'tt')
                      ->fields('tt')
                      ->condition('timer_id', $timer_id)
                      ->execute();

  $timer = $timer_query_result->fetchObject();
  $timer->start = $start_time;
  $timer->finish = $finish_time;

  return tasks_and_timers_build_confirm_row($timer);
}

/**
 * Menu Page Callback
 */
function tasks_and_timers_confirm_user_time_page($user) { 

  // Set title for page (I cant quite get the 'menu regirstry' way
  // for getting this done)
  drupal_set_title(t('Confirm Time for @username', 
    array('@username' => $user->name)));
  drupal_add_js(drupal_get_path('module', 'tasks_and_timers') . 
    '/js/tasks_and_timers-confirm-page.js');

  // Collect the UNCONFIRMED timer entries for the given user
  $timers = TaskCalculator::getUserUnconfirmedTimers($user->uid);
  
  $output = '';

  if ($timers) { 

    $output .= '<div id="confirm-timers-section">';

    foreach ($timers as $timer) { 

      $output .= tasks_and_timers_build_confirm_row($timer);
    }

    $output .= '</div>';
  }

  else { 

    drupal_set_message('There are no timers to display', NULL, 'warning');
  }

  return $output;
}

/**
 * Private prototype helpers - future implementations may integrate with views
 * so I'm holding back on integrating with theme layer just yet.  For right now
 * this function is in place to help prove the concept more than anything.
 */
function tasks_and_timers_build_confirm_row($timer_row) { 

  // Determine some data about timer
  $timer = new Timer($timer_row->timer_id);
  $timer_name = $timer->getTask()->title;

  $task_collection = TaskCalculator::collectTimers($timer_row->task_id, 
    $timer_row->owner_uid);

  $task_total_time = TaskCalculator::sum($task_collection);
  $task_total_time -= $timer->getTimePassed();

  $passed_time = $timer_row->finish - $timer_row->start;
  $task_total_time += $passed_time;

  // Build confirmation forms
  $start_time_form = drupal_get_form('tasks_and_timers_timer_confirm_form', 
    $timer_row->start, $timer_row->timer_id, 'start_time');
  $finish_time_form = drupal_get_form('tasks_and_timers_timer_confirm_form',
    $timer_row->finish, $timer_row->timer_id, 'finish_time');

  // Define output
  $markup = array();

  $output = '';

  $output .= '<table id="' . $timer_row->timer_id . '_timer_table">';
  
  $output .= '<tr><th>Row ID</th><th>Task Name</th><th>Start Time</th>' . 
             '<th>Stop Time</th><th>Timer Total</th><th>Task Total</th>' . 
             '<th>Confirm</th><th>Remove</th></tr>';
  $output .= '<tr>';
  $output .= '<td>' . $timer_row->timer_id . '</td>';
  $output .= '<td>' . $timer_name . '</td>';

  $output .= '<td>' . drupal_render($start_time_form) . '</td>';

  $output .= '<td>' . drupal_render($finish_time_form) . '</td>';

  $output .= '<td class="timer_elapsed_time">' . 
    tasks_and_timers_convert_elapsed_time($passed_time) . '</td>';
  $output .= '<td class="task_total_time">' . 
    tasks_and_timers_convert_elapsed_time($task_total_time) . '</td>';

  $output .= '<td><span class="confirm-link">Confirm<span></td>';

  $output .= '<td><span class="remove-link">Remove</span></td>';

  $output .= '</table>';

  // Return results
  return $output;
}

/**
 * Some private helpers
 */

// Feed in an array with H, M, S in an array to get a string back
function _append_time_string($timer, &$str) { 

  foreach($timer->time as $key => $val) { 

    if ($key < 2) { 
      $str .= (string) $val . ":";
    }
    else { 
      $str .= (string) $val . " ";
    }
  }
}

// Feed in an array with M, D and Y in an array to get a string back
function _append_date_string($timer, &$str) { 

  foreach($timer->date as $key => $val) { 

    if($key < 2) { 

      $str .= (string) $val . "/";
    }
    else {

      $str .= (string) $val;
    } 
  }
}

/**
 * AJAX Forms Callbacks
 */

function tasks_and_timers_update_available_projects(&$form, &$form_state) { 

  $new_options = 
    tasks_and_timers_get_current_project_phase_options($form, $form_state);

  $form['field_task_phase'][LANGUAGE_NONE]['#options'] = $new_options;

  return $form['field_task_phase'];
} 

function tasks_and_timers_update_tasks_view(
  &$form, &$form_state) { 

  $a = 1;

  $new_options = 
    tasks_and_timers_get_current_project_phase_options($form, $form_state, TRUE);

  $form['field_task_phase_tid']['#options'] = $new_options;

  return $form['field_task_phase_tid'];
} 
