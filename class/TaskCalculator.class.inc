<?php
/**
 * @file
 * Contains all code for the TaskCalculator Class
 *
 * The TaskCalculator is responsible for tallying up
 * statistics about timers and returning useful information.
 */

/**
 * @class
 * Reads Tasks, performs computations on task data.
 */
class TaskCalculator {

  protected $taskNID;

  /**
   * Get user's unconfirmed timers.
   *
   * @param int $uid
   *   The uid of the user whose time we're collecting.
   *
   * @return array
   *   Returns a nested array of timer records
   *   Returns NULL when no results are found.
   */
  public static function getUserUnconfirmedTimers($uid) {

    // Validate input before operation. Throw exception if the
    // user doesn't exist.
    if (!user_load($uid)) {

      throw new Exception('User with uid ' . check_plain($uid) . ' not found,' .
        ' cannot collect uncofirmed time for non-existent user.');
    }

    // Create result storage array.
    $result_storage = array();

    // Query Database for result.
    // Collect all fields from the tt_timers table where
    // the timers haven't been confirmed yet.
    $result = db_select('tt_timers', 'tt')
           ->fields('tt')
           ->condition('finish', 0, '>')
           ->isNull('confirmed_by_uid')
           ->execute();

    // Assign results to result storage.
    if ($result) {

      while ($record = $result->fetchObject()) {

        $result_storage[$record->timer_id] = $record;
      }
    }

    // Return result OR NULL if no results found.
    return (empty($result_storage)) ? NULL : $result_storage;
  }

  /**
   * Collect all timers belonging to a Task Node.  Optional filter by user.
   *
   * @param int $task_nid
   *   The nid of the node to inspect against.
   * @param int $user_id
   *   (optional) The user timers to collect. Leave empty to collect all timers.
   *
   * @return array
   *   Returns an array of Timer objects belonging to the tasks's node and 
   *   the indicated user (or all timers for the node with no params).
   *   Returns NULL if nothing was found.
   */
  public static function collectTimers($task_nid, $user_id = NULL) {

    // Create result delivery array.
    $result_array = array();

    // Validate Inputs.  If the task or the user IDs don't load, throw
    // an exception.
    if (!node_load($task_nid)) {

      throw new Exception('Could not create task with task nid ' .
        $task_nid . ' - the nid is invalid.');
    }

    if (!user_load($user_id)) {

      throw new Exception('No user could be found with user ID ' . $user_id);
    }

    // Query for all timer rows that are attributed to the given task id.
    $query = db_select('tt_timers', 'tt')
               ->fields('tt', array('timer_id'))
               ->condition('task_id', $task_nid);

    // If a user id was passed in, narrow the search to timer rows belonging
    // to the given user.
    if ($user_id) {

      $query->condition('owner_uid', $user_id);
    }

    // Execute query and move through records.  Convert each record.
    $result = $query->execute();

    while ($record = $result->fetchObject()) {

      $result_array[$record->timer_id] = new Timer($record->timer_id);
    }

    // If the result array has contents, ship it back.  Return NULL otherwise.
    if (!empty($result_array)) {

      return $result_array;
    }

    return NULL;
  }

  /**
   * Sums up the elapsed time on a collection of Timers and returns result.
   *
   * @param array $collection
   *   An array of Timer objects.  Each object will have its elapsed time 
   *   added to the collective sum.
   * @param bool $require_confirmed
   *   (optional) When TRUE is passed in, sum() will only take 
   *   confirmed timers into account.  Unconfirmed timers are ignored
   *   in this case.
   *
   * @return int 
   *   The sum of elapsed time for given tasks, in seconds.
   */
  public static function sum($collection, $require_confirmed = FALSE) {

    // Validate inputs.
    // If the collction is empty, return 0.
    if (empty($collection)) {

      return 0;
    }

    // Initialze the time accumulator.
    $running_timer = 0;

    // Collect all the seconds recorded as having passed.
    foreach ($collection as $timer_id => $timer) {

      $timer = new Timer($timer_id);

      $running_timer += $timer->getTimePassed($require_confirmed);
    }

    // Return results to caller.
    return $running_timer;
  }

  /**
   * Given a date range and a user id (uid), reports elapsed task time.
   *
   * Feed this function a UNIX start and end date and a uid, 
   * and it will return the amount of time that user has spent on all
   * tasks in that time range.  You can optionally feed in a node id
   * as well, which will restrict the querty to one specific Task.
   * 
   * @param int $start_date
   *   The start date/time, in UNIX timestamp format.
   * @param int $end_date 
   *   The end date/time, in UNIX timestamp format.
   * @param int $uid
   *   The user id of the User in question
   * @param int $nid
   *   (optional) The node id of the Task in quetion
   * 
   * @return array
   *   Returns a nested array.  The key is the task Id, and the next layer
   *   down has information about the sum time, title and nid for that task.
   *   Also has a special 'sum' key at the first level, whose value is 
   *   the sum total of all tasks in that time.
   */
  public static function sumRecordsFromDateRange($start_date,
    $end_date, $uid, $nid = FALSE) {

    // Create container for result delivery.
    $result_array = array();

    // Query for timer rows that start after the start date,
    // end after the end date, and that are owned by the
    // given owner.
    $query = db_select('tt_timers', 'tt')
             ->fields('tt')
             ->condition('start', $start_date, '>=')
             ->condition('finish', $end_date, '<=')
             ->condition('owner_uid', $uid, '=');

    // If a node id was fed in, and that node doesn't exist,
    // throw an exception.
    if ($nid && !node_load($nid)) {

      throw new Exception('Cannot pull statistics for task with nid ' . $nid
        . ' because the node does not exist');
    }

    // If NID is supplied, refine the query to pull only results for that node.
    if ($nid) {

      $query->condition('task_id', $nid, '=');
    }

    // Execute the query.
    $result = $query->execute();

    // If there was a query result, traverse through each row and
    // add the result to the result array.
    if ($result) {

      // Create a variable to keep track of accummulated time.
      $sum_time = 0;

      while ($record = $result->fetchObject()) {

        // If we're looking at a timer with a task we haven't seen yet,
        // we'll put a little array together to track the node title,
        // node id, and sum for the particular Task.
        if (node_load($record->task_id) &&
            !array_key_exists($record->task_id, $result_array)) {

          $result_array[$record->task_id]['title']
            = node_load($record->task_id)->title;
          $result_array[$record->task_id]['nid'] = $record->task_id;
          $result_array[$record->task_id]['sum'] = 0;
        }

        // Create a new timer and add the passed time to the sum for the task.
        // Put this result back in the result array.
        $timer = new Timer($record->timer_id);
        $result_array[$record->task_id]['sum'] += $timer->getTimePassed();
        $sum_time += $timer->getTimePassed();
      }

      $result_array['sum'] = $sum_time;
    }

    // Return results to caller (if there were any).  Return NULL for empty
    // results.
    if (!empty($result_array)) {

      return $result_array;
    }

    return NULL;
  }
}
