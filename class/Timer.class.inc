<?php
/**
 * @file
 * Timer Class File
 *  
 * Timer represents a span of time, associated with one user and one task.
 */

/**
 * @class 
 *   Timer
 * 
 * The Timer class records starting times, 
 * ending times and confirmation states, 
 * as well as associateing the time with a user and task.
 */
class Timer {

  protected $id;
  protected $comment;
  protected $taskID;
  protected $finishTime;
  protected $startTime;
  protected $owner;
  protected $confirmationUID;
  protected $confirmedTime;

  /**
   * Public Constructor.
   * 
   * Load the timer object.
   *
   * @param int $timer_id 
   *   (optional) Pass in a timer ID to load it up.  Will create an empty timer
   *   if no parameter is passed.
   *
   * @throws Exception
   */
  public function __construct($timer_id = NULL) {

    if ($timer_id) {

      try {

        $this->loadTimer($timer_id);
      }

      catch (Exception $ex) {

        drupal_set_message(t('Timer with Timer ID :tid could not be loaded.',
            array(':tid' => $timer_id)),
          'warning'
        );

        watchdog('Tasks and Timers',
        'Timer with Timer ID :tid could not be loaded',
          array(':tid' => $timer_id),
        WATCHDOG_WARNING);
      }
    }
  }

  /**
   * Statically generates a new Timer object. 
   *
   * @param int $uid
   *   The user to whom the Timer belongs.
   * @param int $nid
   *   The node to which the Timer belongs.
   *  
   * @return Timer
   *   Returns a new Timer object.  If there is already an active timer 
   *   on this task, that active timer will be returned instead.
   */
  public static function startNewTimer($uid, $nid) {

    if ($active = Timer::getActiveTimer($uid)) {
      if ($active->getTask()->nid == $nid) {

        return Timer::getActiveTimer($uid);
      }
    }

    $timer = new Timer();
    $timer->setOwner($uid);
    $timer->setTask($nid);
    $timer->start();

    return $timer;
  }

  /**
   * Returns the active Timer object for the given user.
   *
   * @param int $uid
   *   The user id of the User to whom the new Timer will 
   *   belong.
   *
   * @return Timer
   *   Returns the active Timer for the given user, if it
   *   exists.  If there is no active timer, NULL will be 
   *   returned.
   */
  public static function getActiveTimer($uid) {

    $query = db_select('tt_timers', 'tt')
             ->fields('tt', array('timer_id'))
             ->condition('owner_uid', $uid, '=')
             ->isNull('finish')
             ->execute()
             ->fetchObject();

    if ($query && $timer_id = $query->timer_id) {

      $timer_id = $query->timer_id;
      return new Timer($timer_id);
    }

    return NULL;
  }

  /**
   * Set the start time on this Timer.
   *
   * @param int $timestamp
   *   UNIX timestamp of time to start with
   * 
   * @return bool
   *   Reports TRUE if succesful, FALSE if not.
   */
  public function setStartTime($timestamp) {

    if (!is_numeric($timestamp)) {

      return FALSE;
    }

    $this->startTime = $timestamp;
    return TRUE;
  }

  /**
   * Get the declared start time for this Timer.
   *
   * @return int
   *   The declared start time for this Timer as UNIX timestamp
   */
  public function getStartTime() {

    return $this->startTime;
  }

  /**
   * Set the finish time on this timer.
   *
   * @param int $timestamp
   *   UNIX timestamp of time to start with
   * 
   * @return bool
   *   Reports TRUE if succesful, FALSE if not.
   */
  public function setFinishTime($timestamp) {

    if (!is_numeric($timestamp)) {

      return FALSE;
    }

    $this->finishTime = $timestamp;
    return TRUE;
  }

  /**
   * Reports the declared time of this Timer's closing.
   * 
   * @return int
   *   The declared time of this Timer's closing as UNIX timestamp.
   */
  public function getFinishTime() {

    return $this->finishTime;
  }

  /**
   * Delete this Timer.
   */
  public function delete() {

    $query = db_delete('tt_timers')
           ->condition('timer_id', $this->id)
           ->execute();
  }

  /**
   * Return the timer id for this Timer.
   *
   * @return int
   *   The timer id for this Timer.
   */
  public function getTimerID() {

    return $this->id;
  }

  /**
   * Reports with amount of time elapsed from start to finish.
   *
   * If timer is still active, it will report back with the 
   * time from start to present.
   * 
   * @param bool $require_confirmed
   *   (optional) Pass in TRUE to exclude this timer if it must be confirmed.
   * 
   * @return int
   *   Returns the amount of time passed on this timer, in seconds.
   */
  public function getTimePassed($require_confirmed = FALSE) {

    if ($require_confirmed) {

      if (!$this->confirmationUID) {

        return 0;
      }
    }

    if ($this->isActive()) {

      $start_time = $this->startTime;
      $now = time();
      return $now - $start_time;
    }

    if ($this->isFinished()) {

      $start_time = $this->startTime;
      $finish_time = $this->finishTime;
      return $finish_time - $start_time;
    }
  }

  /**
   * Publish a comment about the Timer itself.
   * 
   * @param string $comment
   *   A comment to leave on the Timer.
   *
   * @throws Exception
   */
  public function setComment($comment) {

    if (!$this->id) {

      throw new Exception('Cannot set a comment on an unstarted timer');
    }

    $this->comment = $comment;
  }

  /**
   * Confirm the Timer.
   *
   * @param int $confirm_uid
   *   The user ID of the confirmer.
   */
  public function confirm($confirm_uid) {

    if ($this->id && $this->taskID &&
        $this->finishTime && $this->startTime && $this->owner) {

      if ($this->confirmationUID) {

        throw new Exception("This timer has already been confirmed");
      }

      if ($user = user_load($confirm_uid)) {

        $this->confirmationUID = $confirm_uid;
        $this->confirmedTime = ($this->finishTime - $this->startTime);
        $this->save();
      }
    }
  }

  /**
   * Reports with confirmation status on the Timer.
   *
   * @return bool
   *   Returns TRUE if the Timer has been confirmed, FALSE if not.
   */
  public function isConfirmed() {

    $result = db_select('tt_timers', 'tt')
              ->fields('tt', array('confirmed_by_uid'))
              ->condition('timer_id', $this->id, '=')
              ->execute()
              ->fetchObject();

    if ($result) {

      return $result->confirmed_by_uid;
    }

    return FALSE;
  }

  /**
   * Reports with activity status of the Timer.
   * 
   * @return bool
   *   Returns TRUE if the current timer is active, FALSE if not.
   */
  public function isActive() {

    if (!$this->id || !$this->owner || !$this->taskID) {

      return NULL;
    }

    $result = db_select('tt_timers', 'tt')
              ->fields('tt', array('start', 'finish'))
              ->condition('timer_id', $this->id, '=')
              ->execute()
              ->fetchObject();

    if ($start = $result->start && !$result->finish) {

      return TRUE;
    }
  }

  /**
   * Reports whether or not this Timer has been stopped.
   *  
   * @return bool
   *   Returns TRUE if the Timer has been started and stopped.  FALSE if not.
   */
  public function isFinished() {

    if (!$this->id || !$this->owner || !$this->taskID) {

      return FALSE;
    }

    $result = db_select('tt_timers', 'tt')
              ->fields('tt', array('finish'))
              ->condition('timer_id', $this->id, '=')
              ->execute()
              ->fetchObject();

    if ($result->finish) {

      return TRUE;
    }

    return FALSE;
  }

  /**
   * Set the owner of this Timer.
   *
   * @param int $uid
   *   A user ID representing the user who started this timer
   * 
   * @throws Exception
   */
  public function setOwner($uid) {

    if ($user = user_load($uid)) {

      if ($this->confirmationUID) {

        throw new Exception('This timer has been confirmed.  No further ' .
          'actions may be taken');
      }

      $this->owner = $user;
    }

    else {

      throw new Exception('Must pass in valid user ID');
    }
  }

  /**
   * Returns Drupal User object the owner of This timer.
   *
   * @return stdClass
   *   The Drupal User object representing the owner of this Timer.
   */
  public function getOwner() {

    return $this->owner;
  }

  /**
   * Set the Task ID (node id) for this Timer.
   *
   * @pre: Owner must already be set on this Timer.
   * 
   * @param int $nid
   *   The node ID to reference
   */
  public function setTask($nid) {

    // Check precondition.
    if (!$this->owner) {

      throw new Exception('Owner must be set before defining target node');
    }

    // Verify that the call is valid by making sure that it hasn't been
    // confirmed and that the node id is valid.
    if ($this->confirmationUID) {

      throw new Exception('This timer has been confirmed.  No further ' .
        'actions may be taken');
    }

    if ($node = node_load($nid)) {

      $this->taskID = $nid;
    }

    else {

      throw new Exception("Must pass in a valid node ID");
    }
  }

  /**
   * Get the Task (node) associated with this timer.
   * 
   * @return stdClass
   *   Returns the Drupal Node Object attributed as this Timer's Task.
   */
  public function getTask() {

    if (!$this->taskID || !$node = node_load($this->taskID)) {

      throw new Exception("No valid Task Node is Loaded, cannot return Task");
    }

    return node_load($this->taskID);
  }

  /**
   * Start this timer.
   *
   * @pre:  Owner and taskID must be defined
   * @pre:  Timer instance must be empty
   */
  public function start() {

    // Enforce pre-conditions on the method.
    if (!$this->taskID || !$this->owner) {

      throw new Exception('Task Owner or Task ID not defined - ' .
        'cannot start timer');
    }

    if ($this->startTime) {

      throw new Exception('Cannot restart timer instance once timer ' .
        'has been started');
    }

    if ($this->confirmationUID) {

      throw new Exception('This timer has been confirmed.  No further ' .
        'actions may be taken');
    }

    // Stop any other active timers.
    $active_timer = Timer::getActiveTimer($this->owner->uid);
    if ($active_timer) {

      $active_timer->stop();
    }

    // Set the start time and commit record to db.
    $this->startTime = time();
    $this->save();
    drupal_set_message(t('Timer Started:') . ' ' .
      check_plain(node_load($this->taskID)->title));
  }

  /**
   * Stop the timer.
   *
   * @throws Exception
   */
  public function stop() {

    if ($this->confirmationUID) {

      throw new Exception('This timer has been confirmed.  No further ' .
        'actions may be taken');
    }

    else {

      $this->finishTime = time();
      $this->save();
      drupal_set_message(t('Timer Stopped:') . ' ' .
        check_plain(node_load($this->taskID)->title));
      return;
    }

    //todo throw hook call to affcect nodes

    throw new Exception($state_id . 'is not a valid state ID. ' .
     'Timer was not stopped');
  }


  /**
   * Stores the current Timer object state to the database.
   * 
   * @throws Exception
   */
  public function save() {

    if ($this->isConfirmed()) {

      throw new Exception('Timer has already been confirmed.  Cannot ' .
        'overwrite confirmed record.');
    }

    if ($this->id) {

      db_update('tt_timers')
           ->fields(array(
              'comment' => $this->comment,
              'task_id' => $this->taskID,
              'finish' => $this->finishTime,
              'start' => $this->startTime,
              'owner_uid' => $this->owner->uid,
              'confirmed_by_uid' => $this->confirmationUID,
              'confirmed_time' => $this->confirmedTime))
           ->condition('timer_id', $this->id, '=')
           ->execute();
    }

    elseif ($this->owner && $this->taskID && $this->startTime) {

      // Add this new record to the database.
      db_insert('tt_timers')
        ->fields(array('comment', 'task_id',
          'finish', 'start', 'owner_uid',
          'confirmed_by_uid',
        ))
        ->values(array(
           'comment' => $this->comment,
           'task_id' => $this->taskID,
           'finish' => $this->finishTime,
           'start' => $this->startTime,
           'owner_uid' => $this->owner->uid,
           'confirmed_by_uid' => $this->confirmationUID,
           'confirmed_time' => $this->confirmedTime,
        ))
        ->execute();

      $new_record = Timer::getActiveTimer($this->owner->uid);
      $this->id = $new_record->getTimerID();
    }

    else {

      throw new Exception('Unable to write timer record');
    }
  }

  /**
   * Loads database info into Timer.
   * 
   * Use this function to load up the timer statistics from the 
   * database into the current Timer object instance.
   *
   * @param int $timer_id
   *   The Timer ID of the desired timer
   *
   * @throws Exception when timer id can't be found or database can't
   * load the object up.
   */
  protected function loadTimer($timer_id) {

    $result = db_select('tt_timers', 'tt')
              ->fields('tt')
              ->condition('timer_id', $timer_id, '=')
              ->execute()
              ->fetchObject();

    if ($result) {

      // Load it up.
      $this->id = $result->timer_id;
      $this->comment = $result->comment;
      $this->taskID = $result->task_id;
      $this->finishTime = $result->finish;
      $this->startTime = $result->start;
      $this->owner = user_load($result->owner_uid);
      $this->confirmationUID = $result->confirmed_by_uid;
      $this->confirmedTime = $result->confirmed_time;
    }
  }
}
