<?php

class views_handler_field_elapsed_time extends views_handler_field { 

  //@override
  function query() { 

    $this->ensure_my_table();
    $params = $this->options['group_type'] != 'group' ? array('function' => $this->options['group_type']) : array(); 
    $this->field_alias = $this->query->add_field($this->table_alias, $this->real_field, NULL, $params); 
 
    $this->add_additional_fields(); 
  }

  function render($values) { 

    $value = format_interval($this->get_value($values), 2);

    return $value;
  }
}
